JSON.parse('{}')              // {}
JSON.parse('true')            // true
JSON.parse('"foo"')           // "foo"
JSON.parse('[1, 5, "false"]') // [1, 5, "false"]
JSON.parse('null')            // null

JSON.parse('{"p": 5}', function (k, v) {
  if (k === '') { return v } // if topmost value, return it,
  return v * 2               // else return v * 2.
})                           // { p: 10 }

JSON.parse('{"1": 1, "2": 2, "3": {"4": 4, "5": {"6": 6}}}', function (k, v) {
  console.log(k) // log the current property name, the last is "".
  return v       // return the unchanged property value.
})

// 1
// 2
// 4
// 6
// 5
// 3
// ""


// parse and revive into an object //
var myObj = JSON.parse('{"1": 1, "2": 2, "3": {"4": 4, "5": {"6": 6}}}', function (k, v) {
  if (k === '2') {
    return v * 100
  } else if (k === '') {
    return v
  }
})

console.log(myObj)


// stringify //
JSON.stringify({})                  // '{}'
JSON.stringify(true)                // 'true'
JSON.stringify('foo')               // '"foo"'
JSON.stringify([1, 'false', false]) // '[1,"false",false]'
JSON.stringify({ x: 5 })            // '{"x":5}'

JSON.stringify({ x: 5, y: 6 })
// '{"x":5,"y":6}' or '{"y":6,"x":5}'
JSON.stringify([new Number(1), new String('false'), new Boolean(false)])
// '[1,"false",false]'

// Symbols:
JSON.stringify({ x: undefined, y: Object, z: Symbol('') })
// '{}'
JSON.stringify({ [Symbol('foo')]: 'foo' })
// '{}'
JSON.stringify({ [Symbol.for('foo')]: 'foo' }, [Symbol.for('foo')])
// '{}'
JSON.stringify({ [Symbol.for('foo')]: 'foo' }, function (k, v) {
  if (typeof k === 'symbol') {
    return 'a symbol'
  }
})
// '{}'

// Non-enumerable properties:
JSON.stringify(Object.create(null, { x: { value: 'x', enumerable: false }, y: { value: 'y', enumerable: true } }))
// '{"y":"y"}'


// replacer in stringify //
function replacer (key, value) {
  if (typeof value === 'string') {
    return undefined
  }
  return value
}

var foo = {foundation: 'Mozilla', model: 'box', week: 45, transport: 'car', month: 7}
var jsonString = JSON.stringify(foo, replacer)
