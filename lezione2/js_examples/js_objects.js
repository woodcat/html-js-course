var obj = {
  a: 'hello world',
  b: 42,
  c: true
}

obj.a      // "hello world"
obj.b      // 42
obj.c      // true

obj['a']   // "hello world"
obj['b']   // 42
obj['c']   // true

var name = 'a'
obj[name]  // "hello world"


// array
var arr = [
  'hello world',
  42,
  true
]

arr[0]         // "hello world"
arr[1]         // 42
arr[2]         // true
arr.length     // 3

typeof arr     // "object"


// function //
function foo () {
  return 42
}

foo.bar = 'hello world'

typeof foo         // "function"
typeof foo()       // "number"
typeof foo.bar     // "string"


// built in methods //
var a = 'hello world'
var b = 3.14159

a.length               // 11
a.toUpperCase()        // "HELLO WORLD"
b.toFixed(4)           // "3.1416"
