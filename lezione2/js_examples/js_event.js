var event = new Event('build')

var elem = document.getElementsByTagName('body')[0]

// Listen for the event.
elem.addEventListener('build', function (e) { alert('event listened') }, false)

// Dispatch the event.
elem.dispatchEvent(event)


// event simulation //
function simulateClick () {
  var event = new MouseEvent('click', {
    'view': window,
    'bubbles': true,
    'cancelable': true
  })
  var cb = document.getElementById('checkbox')
  var cancelled = !cb.dispatchEvent(event)
  if (cancelled) {
    // A handler called preventDefault.
    alert('cancelled')
  } else {
    // None of the handlers called preventDefault.
    alert('not cancelled')
  }
}
