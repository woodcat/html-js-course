// explicit coercion //
var a = '42'

var b = Number(a)

a // "42"
b // 42 -- the number!

// implicit coercion
var s = '42'

var n = a * 1 // "42" implicitly coerced to 42 here

s // "42"
n // 42 -- the number!

// truthy and falsy //
!'' // true
!0 // true
!-0 // true
!NaN // true
!null // true
!undefined // true
!false // true

// equality //
var nums = '42'
var numn = 42

nums == numn         // true
nums === numn        // false

var va = [1, 2, 3]
var vb = [1, 2, 3]
var vc = '1,2,3'

va == vc     // true
vb == vc     // true
va == vb     // false
