var foo = {
  a: 42
}

// create `bar` and link it to `foo`
var bar = Object.create(foo)

bar.b = 'hello world'

bar.b      // "hello world"
bar.a      // 42 <-- delegated to `foo`



// estendere una funzione con prototype //
function Persona (n, c) {
  this.nome = n
  this.cognome = c
}


var io = new Persona('marco', 'dussin')
var tu = new Persona('ivano', 'masiero')

console.log(io.nome)
console.log(tu.nome)

Persona.prototype.professione = 'ingegnere'

console.log(io.professione)
console.log(tu.professione)
